corsix-th (0.62-1) UNRELEASED; urgency=medium

  [ Phil Morrell ]
  * add gbp.conf
  * add contrib disclaimer
  * lintian testsuite fix, bump metadata
  * allow more variety in signature files
  * fix dh_auto_test nocheck indentation #902392

  [ Sebastian Ramacher ]
  * New upstream version 0.62
  * debian/: Make cmake install files in correct location.
  * debian/copyright: Update copyright info
  * debian/control:
    - Recommend timidity. (Closes: #904838)
    - Bump Standards-Version.
    - Update ffmpeg Build-Depends according to build system.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 29 Jul 2018 13:41:56 +0200

corsix-th (0.61-1) unstable; urgency=medium

  [ Alexandre Detiste ]
  * New upstream version 0.61

  [ Phil Morrell ]
  * add pedantic lintian overrides
  * use https copyright-format url for policy v4.0.0
  * bump Standards-Version, no changes needed
  * add upstream signature to verify releases
  * drop patches released upstream
  * update main copyright authors from LICENSE.txt
  * Change Vcs-* to point to salsa.debian.org

 -- Phil Morrell <debian@emorrp1.name>  Thu, 11 Jan 2018 22:42:02 +0000

corsix-th (0.60-2) unstable; urgency=medium

  * remove obsolete rnc.pp copyright comment.
    Thanks to Alexandre Detiste
  * enable unit tests with newly packaged lua-busted
  * more flexible tag matching in debian/watch
  * bump debhelper compat to 10

 -- Phil Morrell <debian@emorrp1.name>  Tue, 15 Nov 2016 18:08:55 +0000

corsix-th (0.60-1) unstable; urgency=medium

  [ Alexandre Detiste ]
  * New upstream release.

  [ Phil Morrell ]
  * drop all non-debian patches merged upstream
  * backport include_campaigns.patch pushed upstream
  * backport editor_drag_performance.patch for usability
  * drop obsolete lintian-overrides
  * bump Standards-Version
  * add new project website URL
  * add copyright for new campaign by ChrizmanTV
  * update main copyright authors from LICENSE.txt

 -- Phil Morrell <debian@emorrp1.name>  Wed, 22 Jun 2016 23:05:26 +0100

corsix-th (0.50-2) unstable; urgency=medium

  * backport upstream patch to build with ffmpeg 3.0 (Closes: #821415)
  * use v4 watch file to include pre-releases
  * wrap-and-sort -satb

 -- Phil Morrell <debian@emorrp1.name>  Mon, 18 Apr 2016 19:20:40 +0100

corsix-th (0.50-1) unstable; urgency=medium

  * Initial release. (Closes: #610087)

 -- Alexandre Detiste <alexandre.detiste@gmail.com>  Mon, 29 Feb 2016 15:05:38 +0100
